1 ---- info_retrieval_system ----

This folder contains the information-retrieval code implemented in Python.

Dependency list:
	-	lxml
	-	bs4
	- 	nltk

The two following scripts can be executed:
	-	ranking_generator.py 	to launch the ranking procedure for a specified document collection
	-	benchmark.py 			to launch the benchmark procedure


2 ---- quiz_game ----

This folder contains the prototype of a quiz videogame, made with the Unity3D game engine.
The game uses the ranking score generated on the document collection to create multiple choice questions
with the required difficulty level. 

To open the project, launch Unity3D and select the folder 'quiz_game/Inforet Quiz Game'.