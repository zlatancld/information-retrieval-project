from vector_space_model import SpaceModel
import my_utilities
import time

# this method take a dict of document and for each doc generates the ranking of k-nearest docs
# the result is a dictionary: doc_id -> [(id_1, score_1), (id_2, score_2), ..., (id_n, score_n)]
def get_similarity_ranking(docs_dict):
    print("Generating similarity for {0} documents".format(len(docs_dict)))
    result = {}

    start_time = time.time()

    # create Vector Space Model and add documents
    vsm = SpaceModel()
    for d in docs_dict.values():
        vsm.add_document(d.id, d.tokens)

    # generate the normalized document vectors
    vsm.calculate_document_vectors(True, True)

    # get the ranking for each document
    for d_id in docs_dict.keys():
        rank = vsm.calculate_rank_similarity(d_id, 10)
        result[d_id] = rank

    elapsed_time = time.time() - start_time

    print("Processed in {0} seconds".format(round(elapsed_time, 2)))

    return result

# it takes the documents and ranking dictionaries and builds the whole structure for each doc:
# doc_id -> id, title, description, image_url, ranking:[(doc_id, score)..]
# the resulted structure is stored in a json file at stated filepath
def store_docs_and_ranking(docs, ranks, filepath):
    print("Storing content and ranking of {0} documents at {1}".format(len(docs), filepath))

    docs_array = []
    for id, doc in docs.items():
        json_doc = {}
        json_doc["id"] = str(id)
        json_doc["title"] = doc.title
        json_doc["sentences"] = doc.sentences
        json_doc["tokens_count"] = len(doc.tokens)
        if doc.image_url is not None:
            json_doc["image_url"] = doc.image_url
        # store the rank as a json list containing json object
        json_doc["rank"] = list(map(lambda x : {"id" : x[0], "score" : round(x[1], 3)}, ranks[id]))

        docs_array.append(json_doc)

    json_result = {"docs" : docs_array}

    my_utilities.write_json_to_file(json_result, filepath)

    print("Storing completed!")

# this method does everything about loading existing games, calculating ranking and storing information into json
def generate_games_ranking():
    data = my_utilities.load_docs_from_jsonlist_file("data/igdb.json", ["id"], ["name"], ["summary"], ["cover", "url"])
    ranks = get_similarity_ranking(data)
    store_docs_and_ranking(data, ranks, "data/ranked_games.json")

def generate_films_ranking():
    data = my_utilities.load_docs_from_jsonlist_file("data/imdb.json", ["imdbID"], ["Title"], ["Plot"], ["Poster"])
    ranks = get_similarity_ranking(data)
    store_docs_and_ranking(data, ranks, "data/ranked_films.json")

def generate_boardgames_ranking():
    data = my_utilities.load_docs_from_jsonlist_file("data/boardgames.json", ["_id"], ["title"], ["description"])
    ranks = get_similarity_ranking(data)
    store_docs_and_ranking(data, ranks, "data/ranked_boardgames.json")



if __name__ == "__main__":

    procedures = [generate_games_ranking, generate_boardgames_ranking, generate_films_ranking]
    labels = ["Videogames", "Boardgames", "Films"]

    display_text = "Document Ranking Generator\nChoose the type of documents you want to rank:\n{0}\n"
    choice_text = "\t{0} :- {1}"

    final_text = display_text.format(
            "\n".join(list(map( lambda idx : choice_text.format(idx, labels[idx]), range(0, len(labels))))))

    choice = int(input(final_text))
    if choice in range(0, len(procedures)):
        procedures[choice]()
