import requests
import my_utilities
from doc_utilities import Document

IGDB_TOKEN = "sTYcJgg6HbLjuR27ZWL-PVW6nnWIrukDsGhEAUM6L4M"
IGDB_FILEPATH = "data/igdb.json"

def download_games(id_from, id_to):
    if id_from >= id_to:
        raise ValueError("id_from = {0} must be less than id_to = {1}".format(id_from, id_to))

    result = []
    for id in range(id_from, id_to):
        try:
            url = 'https://www.igdb.com/api/v1/games/' + str(id)
            payload = {"token" : IGDB_TOKEN}
            json_data = requests.get(url, params=payload).json()
            json_game = json_data["game"]
            has_summary = json_game.get("summary") is not None
            print("{2} - {0} {1}".format(json_game["name"], (has_summary and "OK") or "KO", id))
            if has_summary:
                result.append(json_game)
        except KeyError:
            print("error occurred for {0} game".format(id))
        except requests.exceptions.HTTPError:
            print("request HTTP error occurred")
    my_utilities.add_jsons_to_file(result, IGDB_FILEPATH)

#if __name__ == "__main__":
    # the server rest has limits on daily requests, so I've downloaded 4000 games per day
    # download_games(0, 4000)
    # download_games(4000, 8000)
    # download_games(8000, 10050)