import math
from doc_utilities import extract_tokens

class SpaceModel :

    def __init__(self):
        # inverted index contains raw number of occurrences
        self.inverted_index = {}

        # vectors are normalized
        self.doc_vectors = {}

    def add_document(self, document_id, tokens_list):
        # instantiate dictionary for the doc
        doc_vector = {}

        # store occurrences into inverted_index
        for token in tokens_list:
            # document vector contains a boolean frequency for now
            doc_vector[token] = 1
            if self.inverted_index.get(token) is None:
                self.inverted_index[token] = {}
            # term vector contains raw occurrences
            try:
                self.inverted_index[token][document_id] += 1
            except:
                self.inverted_index[token][document_id] = 1

        self.doc_vectors[document_id] = doc_vector

    def calculate_document_vectors(self, use_idf, normalize_vector):
        # read occurrences from inverted index
        for token in self.inverted_index.keys():
            idf = self.calculate_idf(token)
            for doc in self.inverted_index[token].keys():
                self.doc_vectors[doc][token] = 1 + math.log(self.inverted_index[token][doc])
                # check to use idf
                if use_idf: self.doc_vectors[doc][token] *= idf

        # normalize doc vector, if required
        if normalize_vector:
            for doc_id in self.doc_vectors.keys():
                length = euclidean_length(self.doc_vectors[doc_id].values())
                for term in self.doc_vectors[doc_id].keys():
                    self.doc_vectors[doc_id][term] /= length

    def calculate_rank_similarity(self, doc_target_id, first_k = 50):
        scores = {}
        # loop over the terms of target document, to calculate score for each doc
        for term in self.doc_vectors[doc_target_id].keys():
            # calculate weight for target doc term
            w1 = self.doc_vectors[doc_target_id][term]
            # loop over inverted index for documents containing the term
            for doc_id in self.inverted_index[term].keys():
                if doc_id != doc_target_id:
                    w2 = self.doc_vectors[doc_id][term]
                    try: scores[doc_id] += w1 * w2
                    except: scores[doc_id] = w1 * w2

        return sorted(scores.items(), key=lambda tup : -tup[1])[:first_k]

    def query(self, text, first_k = -1):
        # create the query vector
        query_tokens = extract_tokens(text)
        
        query_v = {}
        for token in query_tokens:
            query_v[token] = 1

        # calculate the score for each doc accessing to the inverted index
        scores = {}
        for term, tfidf in query_v.items():
            w1 = tfidf
            # loop over inverted index for documents containing the term
            try:
                for doc_id in self.inverted_index[term].keys():
                    w2 = self.doc_vectors[doc_id][term]
                    try: scores[doc_id] += w1 * w2
                    except: scores[doc_id] = w1 * w2
            except: pass

        first_k = len(scores.items()) if first_k <= 0 else first_k
        return sorted(scores.items(), key=lambda tup : -tup[1])[:first_k]

    def calculate_idf(self, term):
        return math.log(len(self.doc_vectors) / len(self.inverted_index[term]))

def euclidean_length(vector_values):
    return math.sqrt(sum([x ** 2 for x in vector_values]))
