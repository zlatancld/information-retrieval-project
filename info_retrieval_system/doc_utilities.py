import nltk
import string
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from bs4 import BeautifulSoup
import re

class Document:

    def __init__(self, id, title, description):
        self.id = id
        self.title = title
        self.description = description

        self.image_url = None

        # extract tokens
        self.tokens = extract_tokens(description)

        # create sentences list
        self.sentences = extract_sentences(refine_description(self.title, self.description))

    def add_image_url(self, url):
        self.image_url = url


def extract_tokens(text, stemming=True):
    tokens = tokenizer(text) #the list of tokens, containing stopwords and repetitions
    language = detect_language(tokens)
    tokens = remove_stopwords(tokens, language)
    if stemming:
        tokens = stem(tokens, language)

    return tokens

def tokenizer(text, pattern = None):
    if pattern is None:
        pattern = r"""(?x)           # set flag to allow verbose regexps
              (?:[A-Z]\.)+           # abbreviations, e.g. U.S.A.
              |\d+(?:\.\d+)?%?       # numbers, incl. currency and percentages
              |\w+(?:[-']\w+)*       # words w/ optional internal hyphens/apostrophe
              |(?:[+/\-@&*])         # special characters with meanings
            """

    tokens = nltk.regexp_tokenize(text, pattern)
    tokens = [x.encode('utf-8').decode('utf-8') for x in tokens if x not in string.punctuation]
    return tokens

def detect_language(tokens_list):
    max_language = (None, 0)
    tokens_set = set([x.lower() for x in tokens_list])
    for language in stopwords.fileids():
        stopwords_set = set([x for x in stopwords.words(language)])
        cardinality = len(stopwords_set.intersection(tokens_set))
        if max_language[0] is None or max_language[1] < cardinality :
            max_language = (language, cardinality)
    return max_language[0]

def remove_stopwords(tokens_list, language = None):
    if language is None:
        language = detect_language(tokens_list)

    my_stopwords = set([x for x in stopwords.words(language)])
    return [x.lower() for x in tokens_list if not(x.lower() in my_stopwords)]

def stem(tokens_list, language):
    try:
        stemmer = SnowballStemmer(language)
        return [stemmer.stem(x) for x in tokens_list]
    except ValueError:
        print("The language {0} is not supported by stemming procedure".format(language))
        return tokens_list

def refine_description(title, description):
    # remove html stuff
    description = BeautifulSoup(description, "lxml").text
    # remove new lines
    description = re.sub(r'\n+', " ", description)
    # remove title occurrences
    description =  re.sub('(?i)' + re.escape(title), "it", description)

    return description

def extract_sentences(text):
    pattern = r'(?:(?:[A-Z]\.)|[^!?.])+[\.?!]+'
    sentences = re.findall(pattern, text)
    return sentences

