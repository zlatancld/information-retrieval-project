import json
from doc_utilities import Document

def add_jsons_to_file(to_append_list, filepath, preserve_existing = True):
    print("appending {0} new jsons".format(len(to_append_list)))
    try:
        existing = []
        if preserve_existing:
            try:
                data = open(filepath, "r", encoding="utf8")
                existing = json.load(data)
                data.close()
            except:
                print("a previous version of {0} doesn't exist".format(filepath))

        existing.extend(to_append_list)

        data = open(filepath, "w")
        json.dump(existing, data)
        data.close()
    except:
        print("error occurred while appending new jsons to {0}".format(filepath))

def write_json_to_file(json_object, filepath):
    data = open(filepath, "w")
    try:
        json.dump(json_object, data)
    except:
        print("error dumping json to file")
    data.close()

def load_jsons_from_file(filepath):
    json_data = []

    try:
        data = open(filepath, "r", encoding="utf8")
        json_data = json.load(data)
        data.close()
    except:
        print("error occurred while reading {0} file".format(filepath))

    return json_data

# this method load a collection of documents stored into a json file as a list
# the keys parameters are list of keys to access data also for nested jsons
# it returns a dictionary of id -> document
def load_docs_from_jsonlist_file(filepath, id_keys, title_keys, description_keys, image_keys = None):
    print("Loading documents from json at {0}".format(filepath))

    # first load the list of json from file
    json_list = load_jsons_from_file(filepath)

    print("Loaded {0} documents".format(len(json_list)))

    # use a dictionary for result, and a set of title to avoid unexpected duplicates with different id
    documents = {}
    titles = set()

    for j in json_list:
        # id
        id = get_nested_value_from_json(j, id_keys)
        # title
        title = get_nested_value_from_json(j, title_keys)
        # description
        description = get_nested_value_from_json(j, description_keys)

        if id is not None and title is not None and description is not None:
            # check if the title document exists
            if title not in titles:
                doc = Document(id, title, description)
                # check if there are images
                if image_keys is not None:
                    doc.add_image_url(get_nested_value_from_json(j, image_keys))

                # add title to the set
                titles.add(title)
                # add document to results
                documents[id] = doc

    print("Valid documents are {0}".format(len(documents)))

    return documents

def get_nested_value_from_json(json_obj, keys_list):
    try:
        for key in range(0, len(keys_list) - 1):
            json_obj = json_obj[key]
        return json_obj[keys_list[-1]]
    except:
        return None