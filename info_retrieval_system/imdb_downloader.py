from lxml import html
import requests
from doc_utilities import Document
import my_utilities

IMDB_FILEPATH = "data/imdb.json"

def extract_titles_by_html(url):
    # get the html page
    page = requests.get(url)
    tree = html.fromstring(page.content)
    return tree.xpath('//span[@class="wlb_wrapper"]/@data-tconst')

def download_best_films_titles_2000_2016():
    print("downloading best film titles...")
    BASE_URL = "http://www.imdb.com/search/title?at=0&count=100&groups=top_1000&release_date=2000,2016&sort=user_rating&start={0}"
    titles = []
    counter = 1
    while(counter < 467):
        titles.extend(extract_titles_by_html(BASE_URL.format(counter)))
        counter += 100
    print("downloaded {0} titles".format(len(titles)))
    return titles

def download_best_250_film_titles():
    print("downloading best film titles...")
    BASE_URL = "http://www.imdb.com/chart/top"
    # get the html page
    page = requests.get(BASE_URL)
    tree = html.fromstring(page.content)
    titles = tree.xpath('//div[@class="wlb_ribbon"]/@data-tconst')
    print("downloaded {0} titles".format(len(titles)))
    return titles

def download_films_by_id(films_id):
    result = []
    url = "http://www.omdbapi.com/?i={0}&plot=full&r=json"
    for id in films_id:
        try:
            json_film = requests.get(url.format(id)).json()
            print("downloaded {0} - {1}".format(id, json_film["Title"]))
            result.append(json_film)
        except KeyError:
            print("error occurred for {0} film".format(id))
        except requests.exceptions.HTTPError:
            print("request HTTP error occurred")
    print("downloaded {0} films".format(len(result)))
    my_utilities.add_jsons_to_file(result, IMDB_FILEPATH)


#if __name__ == "__main__":
    # first I download the id of the best films
    # film_ids = download_best_films_titles_2000_2016()
    # film_ids.extend(download_best_250_film_titles())

    # then I download the films definition, stored in imdb.json
    # download_films_by_id(film_ids)