from lxml import etree
import doc_utilities
from vector_space_model import SpaceModel
from os import listdir
from bs4 import BeautifulSoup

wiki_folder = "wiki_benchmark"
wiki_folder_queries = "queries"
wiki_document_filename = "documents.tsv"

evaluation_folder = "evaluation_dataset"
evaluation_documents = "docs-raw-texts"
evaluation_queries = "queries-raw-texts"
evaluation_relevance_judgements = "relevance-judgments.tsv"

parser = etree.XMLParser(ns_clean=True, recover=True, encoding='utf-8')

def read_evaluation_document(filepath):
    # read the xml document
    data = open(filepath, "r", encoding="utf8")
    tree = etree.fromstring(data.read().encode('utf-8'), parser=parser)
    data.close()
    # get the content
    content = BeautifulSoup(etree.tostring(tree.xpath('//raw')[0]), "lxml").text
    # get the id
    id = tree.xpath('//public/@publicId')[0]

    return (id, content)

def launch_benchmark_using_dataset():
    print("Loading documents for evaluation...")
    # load docs
    docs = []
    for filename in listdir("{0}/{1}".format(evaluation_folder, evaluation_documents)):
        (doc_id, text) = read_evaluation_document("{0}/{1}/{2}".format(evaluation_folder, evaluation_documents, filename))
        docs.append(doc_utilities.Document(doc_id, doc_id, text))
    print("Loaded {0} files document for evaluation".format(len(docs)))

    # load queries definition
    queries_id_text = []
    for filename in listdir("{0}/{1}".format(evaluation_folder, evaluation_queries)):
        (query_id, text) = read_evaluation_document("{0}/{1}/{2}".format(evaluation_folder, evaluation_queries, filename))
        queries_id_text.append((query_id, text.replace("\n", "")))

    relevance_judgements = {}
    # load relevance judgements for queries
    doc_file = open("{0}/{1}".format(evaluation_folder, evaluation_relevance_judgements), encoding="utf8")
    for line in doc_file:
        unpacked = line.split("\t")
        query_id = unpacked[0]
        relevances_list_text = unpacked[1].replace("\n", "")
        relevance_list = relevances_list_text.split(",")
        relevance_list = list(map(lambda x : x.split(":")[0], relevance_list))
        relevance_judgements[query_id] = relevance_list
    doc_file.close()

    queries = []
    for (id, text) in queries_id_text:
        queries.append((text, relevance_judgements[id]))

    print("Loaded {0} files query for evaluation".format(len(queries)))

    launch_benchmark(docs, queries)


# the corpus contains the documents to use for retrieving result
# the queries param is a list of (query, relevants), and relevants contain the ordered list of relevant docs
def launch_benchmark(corpus, queries):
    vsm = SpaceModel()
    for doc in corpus:
        vsm.add_document(doc.id, doc.tokens)
    vsm.calculate_document_vectors(True, True)

    total_precision_acc = 0
    # loop over the queries and calculate precision
    query_num = 0
    for query, relevants in queries:
        query_num += 1
        myresults = vsm.query(query)
        # look for retrieve each relevant document between results
        query_precision_acc = 0
        relevants_count = 0
        results_count = 0
        for id, score in myresults:
            results_count += 1
            if id in relevants:
                relevants_count += 1
                local_precision = relevants_count / results_count
                query_precision_acc += local_precision
        query_precision_acc = query_precision_acc / relevants_count if relevants_count != 0 else 0
        total_precision_acc += query_precision_acc
        print("Average precision for query {0} = {1}".format(query_num, query_precision_acc))

    total_precision_acc /= query_num
    print("Mean Average Precision as benchmark result = {0}".format((total_precision_acc)))

def launch_wiki_benchmark():
    print("Loading wiki documents for benchmark...")
    # load docs from wiki file
    docs = []
    doc_file = open("{0}/{1}".format(wiki_folder, wiki_document_filename), encoding="utf8")
    for line in doc_file:
        splitted = line.split("\t", 2)
        doc_id = splitted[0]
        text = "".join(splitted[1:])
        docs.append(doc_utilities.Document(doc_id, doc_id, text))
    doc_file.close()
    print("Loaded {0} files from wiki documents file".format(len(docs)))
    # load queries file
    queries = []
    for filename in listdir("{0}/{1}".format(wiki_folder, wiki_folder_queries)):
        file = open("{0}/{1}/{2}".format(wiki_folder, wiki_folder_queries, filename), "r", encoding="utf8")
        # the first line is the query
        q = file.readline()
        # the others are the result
        relevants = []
        for res in file:
            relevants.append(res.strip("\n"))
        file.close()
        queries.append((q, relevants))

    launch_benchmark(docs, queries)


if __name__ == "__main__":
    launch_benchmark_using_dataset()
    #launch_wiki_benchmark()