﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoaderUI : MonoBehaviour {

    private float timeCollector;
    public float UPDATE_TIME = 0.7f;

    public Image[] points;
    private int pointIndex;

    private static bool isLoading;
    private static bool animationCompleted;
    private static bool loadedCompletedRequest;
    public static void OnLoadingCompleted() {
        loadedCompletedRequest = true;
        if(!animationCompleted) {
            return;
        }

        if(isLoading) {
            isLoading = false;
            SceneManager.LoadScene("home");
        }
    }
    
	void Start () {
        isLoading = true;
        pointIndex = 0;
        animationCompleted = false;
	}
	
	// Update is called once per frame
	void Update () {
        timeCollector += Time.deltaTime;
        if(timeCollector >= UPDATE_TIME) {
            timeCollector = 0f;
            AnimatePoints();
        }
	}

    private void AnimatePoints() {
        if(pointIndex == points.Length) {
            foreach(Image i in points)
                i.enabled = false;
            pointIndex = 0;
            animationCompleted = true;

            if(loadedCompletedRequest)
                OnLoadingCompleted();
        }
        else {
            points[pointIndex].enabled = true;
            pointIndex++;
        }
    }
}
