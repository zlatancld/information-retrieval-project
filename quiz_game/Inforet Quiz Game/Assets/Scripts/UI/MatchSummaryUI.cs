﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MatchSummaryUI : MonoBehaviour {

    public Text score, bestScore, category, difficulty;
    public Button playAgain;

	// Use this for initialization
	void Awake () {
	}
	
	void Start() {
        GameParameters parameters = GameParameters.GetInstance();

        int currentScore = (int)parameters.lastMatch.GetTotalScore();
        score.text = "" + currentScore;

        if(GameProgress.GetInstance().SetNewScore(parameters.lastMatch.cat, parameters.lastMatch.diff, currentScore))
            bestScore.text = "NEW BEST SCORE";
        else
            bestScore.text = "Current Best Score is " + GameProgress.GetInstance().GetBestScore(parameters.lastMatch.cat, parameters.lastMatch.diff);

        category.text = "for " + parameters.lastMatch.cat + " Category";
        difficulty.text = "with " + parameters.lastMatch.diff + " Difficulty";
    }
    

}
