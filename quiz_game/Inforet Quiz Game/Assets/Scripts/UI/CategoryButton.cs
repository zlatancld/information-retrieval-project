﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CategoryButton : MonoBehaviour {

    public Category relatedCategory;

    public void OnClick() {
        GameParameters.GetInstance().selectedCategory = relatedCategory;
        SceneManager.LoadScene("difficulties");
    }
	
}
