﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DifficultyButton : MonoBehaviour {

    public Difficulty relatedDifficulty;

    public void OnClick() {
        GameParameters.GetInstance().selectedDifficulty = relatedDifficulty;
        SceneManager.LoadScene("ingame");
    }

}
