﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GenericUI : MonoBehaviour { 

	public void MoveToScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public void Quit() {
        Application.Quit();
    }
    
}
