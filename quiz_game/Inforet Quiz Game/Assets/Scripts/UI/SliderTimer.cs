﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SliderTimer : MonoBehaviour {

    public Slider slider;
    public Image sliderImage;

    public Color maxTimeColor, minTimeColor;
    public float minTimeForColor = 0.2f;
    public float maxTimeForColor = 0.8f;

    private float totalTime;
    public float passedTime { get; private set; }

    private float timeCollector;
    public float TIME_FOR_SCORE_UPDATE = 0.25f;

    private System.Action OnTimeIsUp;
    private System.Action<float> OnPassedSecond;

    public AudioSource tiktakAudio;
    public float TIME_PERCENTAGE_FOR_TIK_TAK = 0.75f;
    
    public void StartTimer(float time, System.Action OnTimeIsUp, System.Action<float> OnPassedSecond) {
        totalTime = time;
        passedTime = 0f;
        timeCollector = 0f;

        this.OnTimeIsUp = OnTimeIsUp;
        this.OnPassedSecond = OnPassedSecond;

        this.enabled = true;
        sliderImage.enabled = true;
    }

    public void StopTimer() {
        //stop audio
        tiktakAudio.Stop();

        this.enabled = false;
    }

    private void TimeIsUp() {
        sliderImage.enabled = false;
        this.enabled = false;

        //stop audio
        tiktakAudio.Stop();
        
        OnTimeIsUp();
    }

    void Start() {
     //   StartTimer(6f);
    }
	
	// Update is called once per frame
	void Update () {
        if(passedTime == totalTime) {
            TimeIsUp();
            return;
        }
        passedTime = Mathf.Min(passedTime + Time.deltaTime, totalTime);
        slider.value = 1 - (passedTime / totalTime);

        CheckTikTak();

        timeCollector += Time.deltaTime;
        if(timeCollector >= TIME_FOR_SCORE_UPDATE) {
            timeCollector = 0f;
            OnPassedSecond(passedTime);
        }
	}

    private void CheckTikTak() {
        if(!tiktakAudio.isPlaying && passedTime >= totalTime * TIME_PERCENTAGE_FOR_TIK_TAK) {
            tiktakAudio.Play();
        }
    }

    private Color GetColorByRatio(float ratio) {
        if(ratio >= maxTimeForColor)
            return maxTimeColor;
        if(ratio <= minTimeForColor)
            return minTimeColor;
        float value = ratio - minTimeForColor;
        float newMax = maxTimeForColor - minTimeForColor;
        float deltaRatio = value / newMax;
        Color result = maxTimeColor;
        result.r = Mathf.Lerp(minTimeColor.r, maxTimeColor.r, deltaRatio);
        result.g = Mathf.Lerp(minTimeColor.g, maxTimeColor.g, deltaRatio);
        result.b = Mathf.Lerp(minTimeColor.b, maxTimeColor.b, deltaRatio);
        return result;
    }
}
