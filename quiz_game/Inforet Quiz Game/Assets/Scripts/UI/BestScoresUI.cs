﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BestScoresUI : MonoBehaviour {

    public Text easyScore, mediumScore, hardScore;
    public Category targetCategory;

	// Use this for initialization
	void Start () {
        GameProgress progress = GameProgress.GetInstance();
        easyScore.text = progress.HasBestScore(targetCategory, Difficulty.EASY) ?
            "" + progress.GetBestScore(targetCategory, Difficulty.EASY) : "n/d";
        mediumScore.text = progress.HasBestScore(targetCategory, Difficulty.MEDIUM) ?
            "" + progress.GetBestScore(targetCategory, Difficulty.MEDIUM) : "n/d";
        hardScore.text = progress.HasBestScore(targetCategory, Difficulty.HARD) ?
            "" + progress.GetBestScore(targetCategory, Difficulty.HARD) : "n/d";
    }
	
}
