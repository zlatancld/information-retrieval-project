﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class InGameUIManager : MonoBehaviour {

    // the text field containing description
    public Text descriptionText;

    public Button[] choiceButtons;
    private Image[] buttonsRenderer;
    private Text[] buttonsText;

    public Button nextButton, leaveButton;
    private Text nextText;

    public Text scoresText;
    public Text stepperText;
    public Text categoryText;
    public Text potentialScore;

    public SliderTimer timer;

    private bool hasChosenAnswer;

    private Match currentMatch;
    private Question currentQuestion;

    // audio
    public AudioSource sourceAudioFeedback;

    public AudioClip goodAnswerSound, badAnswerSound;

    public Color badAnswer, correctAnswer, alternativeAnswer, defaultColor;

	void Awake () {
        buttonsText = new Text[choiceButtons.Length];
        buttonsRenderer = new Image[choiceButtons.Length];
        int idx = 0;
        foreach(Button b in choiceButtons) {
            // get the text fields
            Text t = b.GetComponentInChildren<Text>();
            Image s = b.GetComponent<Image>();
            if(t == null || s == null)
                throw new System.ArgumentNullException("invalid button");
            int val = idx;
            b.onClick.AddListener(() => OnChoiceClicked(val));

            buttonsRenderer[idx] = s;
            buttonsText[idx++] = t;
        }

        nextButton.onClick.AddListener(OnNextClicked);
        nextText = nextButton.GetComponentInChildren<Text>();
	}

    void Start() {
        CreateNewMatch();
    }

    public void CreateNewMatch() {
        GameParameters par = GameParameters.GetInstance();
        currentMatch = new Match(par.selectedDifficulty, par.selectedCategory, par.questionsPerMatch);

        UpdateCategory();
        UpdateScore();

        SetNewQuestion(currentMatch.GetCurrentQuestion());
    }

    private void SetNewQuestion(Question q) {
        this.currentQuestion = q;

        // set description text
        descriptionText.text = q.questionText;

        // set buttons aspect
        string[] answers = q.GetAnswers();
        if(answers.Length != choiceButtons.Length)
            throw new System.ArgumentException("the answers are " + answers.Length + " but buttons are " + choiceButtons.Length);
        for(int i = 0; i < answers.Length; i++) {
            buttonsText[i].text = answers[i];
            SetButtonColor(i, defaultColor);
        }

        UpdateStepper();

        hasChosenAnswer = false;

        timer.StartTimer(q.timeForAnswer, NoAnswerGiven, UpdatePotentialScoreByTime);
        UpdatePotentialScoreByTime(timer.passedTime);
    }

    private void SetButtonColor(int idx, Color color) {
        buttonsRenderer[idx].color = color;
    }

    private void UpdateStepper() {
        stepperText.text = (currentMatch.turnIndex + 1) + " of " + currentMatch.GetQuestionsCount();
    }

    private void UpdateScore() {
        scoresText.text = "" + currentMatch.GetTotalScore();
    }

    private void UpdateCategory() {
        switch(currentMatch.cat) {
            case Category.VIDEOGAMES:
                categoryText.text = "Videogames";
                break;
        }
    }

    private void OnChoiceClicked(int index) {
        if(hasChosenAnswer) 
            return;

        timer.StopTimer();
        hasChosenAnswer = true;

        bool isCorrect = index == currentQuestion.correctAnswerIdx;

        currentQuestion.RegisterAnswer(index, timer.passedTime);
        currentMatch.GoToNextQuestion();
        potentialScore.text = "" + currentQuestion.acquiredPoints;

        SetButtonColor(index, isCorrect ? correctAnswer : badAnswer);
        if(!isCorrect)
            SetButtonColor(currentQuestion.correctAnswerIdx, alternativeAnswer);

        PlayFeedbackSound(isCorrect);

        UpdateScore();
        ShowNextQuestionInterface();
    }

    private void PlayFeedbackSound(bool success) {
        sourceAudioFeedback.clip = success ? goodAnswerSound : badAnswerSound;
        sourceAudioFeedback.Play();
    }

    private void NoAnswerGiven() {
        hasChosenAnswer = true;

        UpdatePotentialScoreByScore(0);
        SetButtonColor(currentQuestion.correctAnswerIdx, alternativeAnswer);

        PlayFeedbackSound(false);

        currentMatch.GoToNextQuestion();
        ShowNextQuestionInterface();
    }

    private void UpdatePotentialScoreByTime(float elapsedTime) {
        potentialScore.text = "" + currentQuestion.PointsForCorrectAnswerByTime(elapsedTime);
    }

    private void UpdatePotentialScoreByScore(int score) {
        potentialScore.text = "" + score;
    }

    private void ShowNextQuestionInterface() {
        nextText.text = currentMatch.IsCompleted() ? "Finish" : "Next";
        nextButton.gameObject.SetActive(true);
        leaveButton.gameObject.SetActive(true);
    }

    private void OnNextClicked() {
        if(currentMatch.IsCompleted()) {
            OnMatchCompleted();
            return;
        }
        
        // play the new question
        nextButton.gameObject.SetActive(false);
        leaveButton.gameObject.SetActive(false);
        SetNewQuestion(currentMatch.GetCurrentQuestion());
    }

    private void OnMatchCompleted() {
        GameParameters.GetInstance().lastMatch = currentMatch;
        SceneManager.LoadScene("endMatch");
    }

}
