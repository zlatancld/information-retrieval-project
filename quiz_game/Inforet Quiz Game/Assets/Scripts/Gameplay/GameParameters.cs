﻿using UnityEngine;
using System.Collections;

/// <summary>
/// singleton class to choose absolute game parameters
/// </summary>
public class GameParameters : MonoBehaviour {

    private static GameParameters instance;

    [Range(5, 20)]
    public int questionsPerMatch = 10;

    [Range(0.0f, 1.0f)]
    public float medianForEasy = 0.0f;
    [Range(0.0f, 1.0f)]
    public float medianForMedium = 0.1f;
    [Range(0.0f, 1.0f)]
    public float medianForHard = 0.15f;

    public int maxPointsForCorrectAnswer = 10;
    public int minPointsForCorrectAnswer = 2;
    public int pointsForBadAnswer = -3;
    public int charactersPerMinute = 863;
    public int answeringSecondsAfterReading = 5;

    public int maxDescriptionCharacters = 250;

    public Category selectedCategory;
    public Difficulty selectedDifficulty;

    public Match lastMatch;

    public static GameParameters GetInstance() {
        if(instance == null) 
            instance = new GameObject("GameParameters", typeof(GameParameters)).GetComponent<GameParameters>();
        
        return instance;
    }

    // Use this for initialization
    void Awake () {
        if(instance != null)
            Debug.LogWarning("There are multiple GameParameters component into the scene, one is needed");
        instance = this;

        DontDestroyOnLoad(this);
	}
	
}
