﻿using UnityEngine;
using System.Collections.Generic;

public class Question {

    public string questionText { get; private set; }
    private string correctAnswer;
    public float timeForAnswer { get; private set; }
    private float timeForReading;

    public float acquiredPoints { get; private set; }

    private List<string> badAnswers;

    public int correctAnswerIdx { get; private set; }
    
    public Question(string text, string correctAnswer) {
        questionText = text;
        this.correctAnswer = correctAnswer;

        this.badAnswers = new List<string>();

        //calculate timing value
        timeForReading = Mathf.CeilToInt((float)questionText.Length / (GameParameters.GetInstance().charactersPerMinute / 60f));
        timeForAnswer = timeForReading + GameParameters.GetInstance().answeringSecondsAfterReading;

        this.acquiredPoints = 0;
    }

    public void AddBadAnswer(string text) {
        badAnswers.Add(text);
    }

    public string[] GetAnswers() {
        int totalCount = badAnswers.Count + 1;
        string[] result = new string[totalCount];
        correctAnswerIdx = UnityEngine.Random.Range(0, totalCount);
        result[correctAnswerIdx] = correctAnswer;

        int idx = 0;
        for(int i = 0; i < badAnswers.Count; i++, idx++) {
            if(idx == correctAnswerIdx)
                idx++;
            result[idx] = badAnswers[i];
        }

        return result;
    }

    public bool RegisterAnswer(int idx, float answeringTime) {
        bool success = correctAnswerIdx == idx;
        acquiredPoints = success ? PointsForCorrectAnswerByTime(answeringTime) : GameParameters.GetInstance().pointsForBadAnswer;
        return success;
    }

    public int PointsForCorrectAnswerByTime(float time) {
        if(time > timeForAnswer)
            return 0;

        float clampedTime = Mathf.Clamp(time, timeForReading, timeForAnswer);
        GameParameters parameters = GameParameters.GetInstance();
        float t = 1 - (timeForAnswer - clampedTime) / (timeForAnswer - timeForReading);
        return Mathf.CeilToInt(Mathf.Lerp(parameters.maxPointsForCorrectAnswer, parameters.minPointsForCorrectAnswer, t));
    }

    public bool HasAnswer() {
        return acquiredPoints != 0;
    }
    
}
