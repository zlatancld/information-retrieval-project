﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class GameProgress {

    private static readonly string PROGRESS_FILEPATH = "/progress.dat";
    private static GameProgress myInstance;

    [SerializeField]
    private Dictionary<Category, Dictionary<Difficulty, int>> bestScores;

    private GameProgress() {
        bestScores = new Dictionary<Category, Dictionary<Difficulty, int>>();
    }

    public int GetBestScore(Category category, Difficulty target) {
        return bestScores[category][target];
    }

    public bool HasBestScore(Category category, Difficulty target) {
        return bestScores.ContainsKey(category) && bestScores[category].ContainsKey(target);
    }

    public bool SetNewScore(Category category, Difficulty difficulty, int newScore) {
        // check if the required dictionary already exists
        if(!bestScores.ContainsKey(category))
            bestScores[category] = new Dictionary<Difficulty, int>();

        // check if the new score is more than the old one (if exists)
        if(!bestScores[category].ContainsKey(difficulty) || GetBestScore(category, difficulty) < newScore) {
            bestScores[category][difficulty] = newScore;

            // save the progress on file
            SaveProgressOnFile();

            return true;
        }
 
        return false;
    }

    public static GameProgress GetInstance() {
        if(myInstance == null) {
            myInstance = LoadProgressByFile();
            if(myInstance == null)
                myInstance = new GameProgress();
        }

        return myInstance;
    }

    private void SaveProgressOnFile() {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + PROGRESS_FILEPATH);
        bf.Serialize(file, this);
        file.Close();
        Debug.Log("Progress saved successfully");
    }

    public void PrintDebug(Category cat) {        
        if(!bestScores.ContainsKey(cat)) {
            Debug.Log(cat + " doesn't contain progress");
            return;
        }

        Dictionary<Difficulty, int> target = bestScores[cat];

        for(int i = 0; i < 3; i++) {
            Debug.Log("score for " + (Difficulty)i + " = " + (target.ContainsKey((Difficulty)i) ? 
                "" + target[(Difficulty)i] : "none"));
        }
    }

    private static GameProgress LoadProgressByFile() {
        if(!File.Exists(Application.persistentDataPath + PROGRESS_FILEPATH))
            return null;
        
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + PROGRESS_FILEPATH, FileMode.Open);
        GameProgress result = bf.Deserialize(file) as GameProgress;
        file.Close();
        return result;
    }

}
