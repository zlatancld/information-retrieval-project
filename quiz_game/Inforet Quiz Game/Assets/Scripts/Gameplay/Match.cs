﻿using UnityEngine;
using System.Collections;

public class Match {

    public Difficulty diff { get; private set; }
    public Category cat { get; private set; }
    private Question[] questions;

    public int turnIndex { get; private set; }
    
    public Match(Difficulty requiredDifficulty, Category requiredCategory, int questionsCount) {
        diff = requiredDifficulty;
        cat = requiredCategory;

        // retrieve questions according with difficulty and category
        DocsSet origin = DocumentsManager.GetInstance().GetDocumentsByCategory(requiredCategory);
        questions = origin.GetDifferentQuestions(requiredDifficulty, questionsCount);

        // initialize data to manage the match
        turnIndex = 0;
    }

    public int GetQuestionsCount() {
        return questions.Length;
    }

    public float GetTotalScore() {
        float sum = 0;
        for(int i = 0; i < questions.Length; i++)
            sum += questions[i].acquiredPoints;
        return sum;
    }

    public Question GetCurrentQuestion() {
        if(turnIndex == questions.Length)
            throw new System.InvalidOperationException("The questions are completed");

        return questions[turnIndex];
    }

    public void GoToNextQuestion() {
        if(turnIndex == questions.Length)
            throw new System.InvalidOperationException("The questions are completed");

        turnIndex = Mathf.Min(turnIndex + 1, questions.Length);
    }

    public bool IsCompleted() {
        return turnIndex == questions.Length;
    }

}
