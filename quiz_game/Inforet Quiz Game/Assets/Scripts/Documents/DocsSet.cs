﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class DocsSet {

    public RankedDoc[] docs;

    private Dictionary<string, RankedDoc> docsByID;

    // documents grouped by difficulty
    private Dictionary<Difficulty, List<RankedDoc>> docsByDifficulty;
	
    public static DocsSet CreateFromJson(string json) {
        DocsSet result = JsonUtility.FromJson<DocsSet>(json);
        result.Initialize();
        return result;
    }
    
    private void Initialize() {
        docsByID = new Dictionary<string, RankedDoc>();
        docsByDifficulty = new Dictionary<Difficulty, List<RankedDoc>>() {
            { Difficulty.EASY, new List<RankedDoc>() }, { Difficulty.MEDIUM, new List<RankedDoc>() }, { Difficulty.HARD, new List<RankedDoc>() } };
        foreach(RankedDoc d in docs) {
            // initialize doc
            d.Initialize();
            docsByID.Add(d.id, d);

            // check if the document has a valid description, not empty
            if(d.description.Length != 0) {
                // insert into list grouped by difficulty
                foreach(KeyValuePair<Difficulty, List<RankedDoc>> entry in docsByDifficulty) {
                    if(d.FitDifficulty(entry.Key))
                        entry.Value.Add(d);
                }
            }
        }
    }

    private Question CreateQuestionByDocument(RankedDoc doc, Difficulty requiredDifficulty) {
        Question q = new Question(doc.description, doc.title);
        // create question
        foreach(ScoreElement score in doc.GetBadAnswers(requiredDifficulty, 3)) {
            q.AddBadAnswer(docsByID[score.id].title);
        }

        return q;
    }
    
    /// <summary>
    /// Return a list of n different questions created using the document into the set
    /// The questions are chosen accordingly with the difficuty
    /// </summary>
    public Question[] GetDifferentQuestions(Difficulty requiredDifficulty, int count) {
        if(count > docsByDifficulty[requiredDifficulty].Count)
            throw new System.ArgumentException("count is too large respect to the documents");
        List<RankedDoc> candidates = new List<RankedDoc>(docsByDifficulty[requiredDifficulty]);
        Question[] result = new Question[count];
        for(int i = 0; i < result.Length; i++) {
            // choose a random document from the list of fitting list
            int randomIdx = UnityEngine.Random.Range(0, candidates.Count);
            result[i] = CreateQuestionByDocument(candidates[randomIdx], requiredDifficulty);
            candidates.RemoveAt(randomIdx);
        }

        return result;
    }

    public void CalculateScoreRanges(int granularitySlots) {
        float delta = 1f / (float)granularitySlots;
        //Debug.Log("delta = " + delta);
        int[] slots = new int[granularitySlots];
        int[] uniqueDocSlots = new int[granularitySlots];

        // count the number of scores that belong to each slot
        foreach(RankedDoc doc in docs) {
            int[] docSlots = new int[granularitySlots];
            foreach(ScoreElement score in doc.rank) {
                int idx = Mathf.FloorToInt(score.score / delta);
                if(idx >= slots.Length)
                    idx = slots.Length - 1;
                //Debug.Log("score = " + score.score + " and ratio is " + idx);
                slots[idx]++;
                docSlots[idx] = 1;
            }
            for(int i = 0; i < docSlots.Length; i++)
                uniqueDocSlots[i] += docSlots[i];
        }

        //print the result
        for(int i = 0; i < slots.Length; i++)
            Debug.Log(slots[i] + " scores (" + uniqueDocSlots[i] + " uniques) less than " + (delta * (i + 1)));
    }

}
