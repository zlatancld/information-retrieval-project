﻿using UnityEngine;
using System.Collections;

public class DocumentsManager : MonoBehaviour{

    private DocsSet games, films, boardgames;

    public static DocumentsManager instance;

    public static DocumentsManager GetInstance() {
        if(instance == null) 
            instance = new GameObject("Documents Manager", typeof(DocumentsManager)).GetComponent<DocumentsManager>();

        return instance;
    }
    
    void Start() {
        if(instance != null)
            Debug.LogWarning("There are multiple DocumentsManager component into the scene, one is needed");

        instance = this;
        games = LoadDocumentsFromJson("ranked_games");
        films = LoadDocumentsFromJson("ranked_films");
        boardgames = LoadDocumentsFromJson("ranked_boardgames");

        DontDestroyOnLoad(this);

        LoaderUI.OnLoadingCompleted();
    }

    public DocsSet LoadDocumentsFromJson(string filename) {
        TextAsset textAsset = Resources.Load<TextAsset>(filename);
        if(textAsset == null)
            throw new System.InvalidOperationException("the path " + filename + " doesn't contain a json file");
        DocsSet docs = DocsSet.CreateFromJson(textAsset.text);
        return docs;
    }

    public DocsSet GetDocumentsByCategory(Category cat) {
        switch(cat) {
            case Category.VIDEOGAMES:
                return games;
            case Category.FILMS:
                return films;
            case Category.BOARDGAMES:
                return boardgames;
            default: throw new System.ArgumentException("the category " + cat + " is not supported");
        }
    }

}
