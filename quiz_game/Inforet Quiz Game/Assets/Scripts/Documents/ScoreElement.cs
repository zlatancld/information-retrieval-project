﻿using System;
using UnityEngine;

[Serializable]
public class ScoreElement : IComparable<ScoreElement> {
    public string id;
    public float score;

    public static ScoreElement CreateFromJson(string json) {
        return JsonUtility.FromJson<ScoreElement>(json);
    }

    int IComparable<ScoreElement>.CompareTo(ScoreElement other) {
        if(other == null) return 1;

        return score.CompareTo(other.score);
    }
    
}
