﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class RankedDoc {
    public string id;
    public string title;
    public string[] sentences;
    public string image_url;
    public ScoreElement[] rank;

    public string description { get; private set; }

    // the median score of scores distribution
    public float medianScore { get; private set; }
    private int halfUpperIndex;

    public static RankedDoc CreateFromJson(string json) {
        RankedDoc result = JsonUtility.FromJson<RankedDoc>(json);
        result.Initialize();
        return result;
    }

    public void Initialize() {
        title = title.ToUpperInvariant();

        // based on the description limits, create the description adding sentences
        int charsCount = 0;
        int idx = 0;
        while(idx < sentences.Length && charsCount + sentences[idx].Length <
            GameParameters.GetInstance().maxDescriptionCharacters) {
            charsCount += sentences[idx].Length;
            idx++;
        }
        description = String.Join("", sentences, 0, idx);
        description = description.ToUpperInvariant();

        // sort the scores array
        Array.Sort<ScoreElement>(rank);
        // ascendent order
        Array.Reverse(rank);

        // calculate median score
        if(rank.Length % 2 != 0) {
            // get the central value for odd length
            halfUpperIndex = Mathf.FloorToInt(rank.Length / 2f);
            medianScore = rank[halfUpperIndex].score;
        }
        else {
            // the average of 2 central values for even length
            halfUpperIndex = (rank.Length / 2) - 1;
            medianScore = (rank[halfUpperIndex].score + rank[halfUpperIndex + 1].score) / 2f;
        }

        //Debug.Log("median score = " + medianScore);
    }

    public bool FitDifficulty(Difficulty target) {
        switch(target) {
            case Difficulty.HARD:
                return medianScore >= GameParameters.GetInstance().medianForHard;
            case Difficulty.MEDIUM:
                return medianScore >= GameParameters.GetInstance().medianForMedium;
            case Difficulty.EASY:
                return medianScore >= GameParameters.GetInstance().medianForEasy;
            default: return true;
        }
    }

    public List<ScoreElement> GetBadAnswers(Difficulty requiredDifficulty, int count) {
        if(requiredDifficulty != Difficulty.MEDIUM) {
            int minIndex, maxIndex;
            if(requiredDifficulty == Difficulty.EASY) {
                minIndex = halfUpperIndex + 1;
                maxIndex = rank.Length - 1;
            }
            else {
                minIndex = 0;
                maxIndex = halfUpperIndex;
            }
            return GetRandomFromRank(minIndex, maxIndex, count);
        }
        else {
            int firstCount = Mathf.FloorToInt(count / 2f);
            List<ScoreElement> firstHalf = GetRandomFromRank(0, halfUpperIndex, firstCount);
            List<ScoreElement> secondHalf = GetRandomFromRank(halfUpperIndex + 1, rank.Length - 1, count - firstCount);
            return MergeListsRandomly(firstHalf, secondHalf);
        }
    }

    public List<ScoreElement> GetRandomFromRank(int from, int to, int count) {
        if(to < from)
            throw new ArgumentException("from = " + from + " is more than to = " + to);
        if(count > (to - from))
            throw new ArgumentException("count is too large");

        List<ScoreElement> source = new List<ScoreElement>();
        for(int i = from; i <= to; i++)
            source.Add(rank[i]);
        List<ScoreElement> result = new List<ScoreElement>();
        for(int i = 0; i < count; i++) {
            int randomIdx = UnityEngine.Random.Range(0, source.Count);
            result.Add(source[randomIdx]);
            source.RemoveAt(randomIdx);
        }
        return result;
    }

    public static List<ScoreElement> MergeListsRandomly(List<ScoreElement> a, List<ScoreElement> b) {
        foreach(ScoreElement elem in b) 
            a.Insert(UnityEngine.Random.Range(0, a.Count), elem);
        return a;
    }

}
